package com.tio_gt;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.tio_gt.excepciones.IdNoEncontradoException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class FakeServiceTest {

    @Mock
    private FakeBean fakeBean;

    @InjectMocks
    private FakeService fakeService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void calcularImpuestoA() throws Exception {
        when(fakeBean.findByDpi(dpiPersonaA)).thenReturn(personaA);

        final Double calculo = fakeService.calcularImpuesto(dpiPersonaA);

        assertEquals(resultadoA, calculo);
    }

    @Test
    public void calcularImpuestoB() throws Exception {
        when(fakeBean.findByDpi(dpiPersonaB)).thenReturn(personaB);

        final Double calculo = fakeService.calcularImpuesto(dpiPersonaB);

        assertEquals(resultadoB, calculo);
    }

    @Test(expected = IdNoEncontradoException.class)
    public void calcularImpuestoPersonaInexistente() throws Exception {
        when(fakeBean.findByDpi(dpiPersonaC)).thenThrow(new IdNoEncontradoException());

        fakeService.calcularImpuesto(dpiPersonaC);
    }

    //<editor-fold defaultstate="collapsed" desc="Datos estaticos">
    private final long dpiPersonaA = 11223344L;
    private final long dpiPersonaB = 11223346L;
    private final long dpiPersonaC = 11223348L;

    private final FakeDTO personaA = new FakeDTO(dpiPersonaA, "Luis", "Morales", "Zacapa", 10000.00);
    private final FakeDTO personaB = new FakeDTO(dpiPersonaB, "Luis", "Morales", "Zacapa", 18000.00);

    private final Double resultadoA = 500D;
    private final Double resultadoB = 1620D;
    //</editor-fold>

}
