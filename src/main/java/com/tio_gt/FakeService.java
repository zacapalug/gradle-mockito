package com.tio_gt;

/**
 * Clase dummy para simular un servicio.
 * 
 * En esta clase se crea una funcion(la que se le hara testing) que hace uso de funciones propias del bean injectado.
 */
public class FakeService {
    /**
     * Simula la injeccion del bean con la logica para recuperar informacion de la base de datos.
     */
    protected FakeBean fakeBean;

    /**
     * Constante para deuda minima.
     */
    private static final double DEUDA_MINIMA = 15000;

    /**
     * Constante para interes de tipo A.
     */
    private static final double INTERES_A = 0.05;

    /**
     * Constante para interes de tipo B.
     */
    private static final double INTERES_B = 0.09;

    /**
     * Funcion a testear.
     * 
     * Esta funcion revise un ID(dpi), obtiene el registro de este ID junto con su deuda y evalua que interes aplicar.
     * 
     * El objetivo del Unit Testing no es testear el acceso a la base de datos, por lo que esta funcion debe poder testearse con las clases dummys de este proyecto.
     * 
     * @param dpi Identificador del registro
     * 
     * @return el monto luego de aplicar el interes que corresponde
     */
    public double calcularImpuesto(long dpi) {
        final FakeDTO person = fakeBean.findByDpi(dpi);

        double interesAplicar = INTERES_A;

        if (person.getDeuda() > DEUDA_MINIMA) {
            interesAplicar = INTERES_B;
        }

        return person.getDeuda() * interesAplicar;
    }
}
