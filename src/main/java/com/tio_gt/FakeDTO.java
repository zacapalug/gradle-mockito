package com.tio_gt;

/**
 * Clase dummy para simular una Entidad o una clase DTO.
 */
public class FakeDTO {
    private long dpi;
    private String nombre;
    private String apellido;
    private String direccion;
    private double deuda;

    /**
     * Constructor default recomendado para entidades.
     */
    FakeDTO() {
        //Vacio por default
    }

    /**
     * Constructor con todos los parametros necesarios.
     */
    public FakeDTO(long dpi, String nombre, String apellido, String direccion, double deuda) {
        this.dpi = dpi;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.deuda = deuda;
    }

    /**
     * Get {@link #dpi}.
     * 
     * @return {@link #dpi}
     */
    public long getDpi() {
        return dpi;
    }

    /** 
    * Set {@link #dpi}.
    * 
    * @param dpi {@link #dpi} 
    */
    public void setDpi(long dpi) {
        this.dpi = dpi;
    }

    /**
     * Get {@link #nombre}.
     * 
     * @return {@link #nombre}
     */
    public String getNombre() {
        return nombre;
    }

    /** 
    * Set {@link #nombre}.
    * 
    * @param nombre {@link #nombre} 
    */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Get {@link #apellido}.
     * 
     * @return {@link #apellido}
     */
    public String getApellido() {
        return apellido;
    }

    /** 
    * Set {@link #apellido}.
    * 
    * @param apellido {@link #apellido} 
    */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * Get {@link #direccion}.
     * 
     * @return {@link #direccion}
     */
    public String getDireccion() {
        return direccion;
    }

    /** 
    * Set {@link #direccion}.
    * 
    * @param direccion {@link #direccion} 
    */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Get {@link #deuda}.
     * 
     * @return {@link #deuda}
     */
    public double getDeuda() {
        return deuda;
    }

    /** 
    * Set {@link #deuda}.
    * 
    * @param deuda {@link #deuda} 
    */
    public void setDeuda(double deuda) {
        this.deuda = deuda;
    }
}
