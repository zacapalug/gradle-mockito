package com.tio_gt;

import java.util.List;
import java.util.function.Supplier;

/**
 * Clase para simular un bean y su posterior injeccion.
 * 
 * Contine la simulacion de los metodos para obtener datos desde la base de datos.
 */
public class FakeBean {

    /**
     * Variable dummy para simular listado de objetos obtenidos desde la base de datos.
     */
    private List<FakeDTO> fakeDTOS;

    /**
     * Lanzador de exception para utilizarse en conjunto con el metodo findAny y orElseThrow.
     */
    private Supplier<? extends RuntimeException> exception = () -> new RuntimeException("Not found");

    /**
     * Simula la obtencion de todos los objetos de la base de datos.
     */
    public List<FakeDTO> getAll() {
        return fakeDTOS;
    }

    /**
     * Simula la obtencion de un elemento especifico de la base de datos.
     */
    public FakeDTO findByDpi(long dpi) {
        return fakeDTOS.parallelStream().filter(l -> l.getDpi() == dpi).findAny().orElseThrow(exception);
    }
}
