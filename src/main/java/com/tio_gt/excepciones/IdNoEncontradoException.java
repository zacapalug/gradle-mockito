package com.tio_gt.excepciones;

/**
 * Excepcion a lanzar cuando no se encuentra el id solicitado.
 */
public class IdNoEncontradoException extends RuntimeException {

    /**
     * Requerido por Serializable.
     */
    private static final long serialVersionUID = -8554123548665313174L;

	/**
     * Mensaje a mostrar cuando ocurre esta excepcion.
     */
    private static final String MENSAJE = "Usuario no encontrado";

    /**
     * Constructo por default.
     */
    public IdNoEncontradoException() {
        super(MENSAJE);
    }
}